# spring-webmvc-demo

Spring WebMVC demo project

## How to run
    mvn clean verify cargo:run
Check urls:  
Views:  
    http://localhost:8080/hello  
    http://localhost:8080/hello?name=Stas

REST:  
    http://localhost:8080/api/hello  
    http://localhost:8080/api/hello?name=Stas  
