package org.romash.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ContextConfiguration({"classpath:spring-webmvc-root-test.xml"})
@WebAppConfiguration
public class HelloRestControllerTest extends AbstractTestNGSpringContextTests {

    MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;


    @BeforeMethod
    void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetHelloDefaultName() throws Exception {
        mockMvc
                .perform(get("/api/hello"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"message\": \"Hello, Guest!\"}"))
                .andReturn();
    }

    @Test
    public void testGetHelloCustomName() throws Exception {
        mockMvc
                .perform(get("/api/hello?name=Stas"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{\"message\": \"Hello, Stas!\"}"))
                .andReturn();
    }
}
