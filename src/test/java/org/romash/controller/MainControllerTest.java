package org.romash.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration({"classpath:spring-webmvc-root-test.xml"})
@WebAppConfiguration
public class MainControllerTest extends AbstractTestNGSpringContextTests {

    MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;


    @BeforeMethod
    void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetHelloDefaultName() throws Exception {
        mockMvc
                .perform(get("/hello"))
                .andExpect(status().is(200))
                .andExpect(model().attribute("name", "Guest"))
                .andReturn();
    }

    @Test
    public void testGetHelloCustomName() throws Exception {
        mockMvc
                .perform(get("/hello?name=Stas"))
                .andExpect(status().is(200))
                .andExpect(model().attribute("name", "Stas"))
                .andReturn();
    }
}
