package org.romash.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("hello")
public class MainController {

    @GetMapping
//    @RequestMapping(method = RequestMethod.GET)
    public String getHello(@RequestParam(name = "name", defaultValue = "Guest") String name,
                           Model model) {
        model.addAttribute("name", name);
        return "hello";
    }

}
