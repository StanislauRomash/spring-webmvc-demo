package org.romash.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("api/hello")
public class HelloRestController {

    @GetMapping(produces = "application/json")
    public Map<String, String> getHello(@RequestParam(name="name", defaultValue = "Guest") String name,
                                        Model model) {
        Map<String, String> map = new HashMap<>();
        map.put("message", "Hello, " + name + "!");
        return map;
    }
}
